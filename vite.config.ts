import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
// @ts-ignore
import {exec} from 'node:child_process';

export default defineConfig({
	plugins: [sveltekit(), handleZigRefresh()],
});

function handleZigRefresh() {
	return {
		name: "zig-rfr",
		enforce: 'post' as const,
		handleHotUpdate({ file, server }: any) {
			if (file.endsWith(".zig")) {
				console.log("running zig build");
				// @ts-ignore
				exec("sh -c \"cd src/zig && zig build\"", (_, output, err) => {
					if (output) {
						console.log(output);
					}
					if (err) {
						console.log(err);
					}
					if (!output && !err) {
						exec("cp -r src/zig/zig-out/lib static/", () => {
							/* to reload instead:
							server.ws.send({
								type: "full-reload",
								path: "*",
							});
							*/
							server.restart();
							console.log("zig build finished successfully");
						});
					}
				});
			}
		}
	};
}
