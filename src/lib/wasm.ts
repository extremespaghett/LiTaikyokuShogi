// wasm stuff used by both client and server

// either client or server exports
export type DynamicObject = {
    [key: string]: any
}
let wasm_exports: DynamicObject;

let memory_array: Uint8Array;
function updateMemoryArray() {
	memory_array = new Uint8Array(wasm_exports.memory.buffer);
	console.log("note: zig is resizing the memory buffer:", memory_array.length);
}

// only ASCII!!!!
export function zigSliceToStr(ptr: number, size: number) {
	let str = "";
	for (let i = ptr; i < ptr+size; i++) {
		str += String.fromCharCode(memory_array[i]);
	}
	return str;
}

export function zigByteSliceToBuffer(ptr: number, size: number) {
	return new Uint8Array(wasm_exports.memory.buffer, ptr, size);
}

export async function initWasm(buffer: Uint8Array, imports: DynamicObject) {
	var import_object = {
		env: {
			consoleLogJS: (ptr: number, size: number) => {
				console.log("zig:", zigSliceToStr(ptr, size))
			},
			notifyResizeJS: () => {
				updateMemoryArray();
				if (Object.hasOwn(imports, "notifyResizeCustom")) {
					imports.notifyResizeCustom();
				}
			},
		},
	};
	Object.assign(import_object.env, imports);
	let wasm_module = await WebAssembly.instantiate(buffer, import_object);
	wasm_exports = wasm_module.instance.exports;
	let return_value = wasm_exports.main();
	if (return_value != 0) {
		console.log("zig returned error!: " + return_value);
	}
	updateMemoryArray();
	return wasm_exports;
}

export function getPieceJson() {
	const ptr = wasm_exports.getPieceJsonPtr();
	const len = wasm_exports.getPieceJsonLen();
	return zigSliceToStr(ptr, len);
}
