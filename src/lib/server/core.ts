/*
 * core database stuff
 */

 // TODO: handle anonymous/not logged in users

import {PrismaClient} from '@prisma/client'

import type {Cookies} from '@sveltejs/kit';

import { error } from '@sveltejs/kit';

import {initWasm, getPieceJson} from "$lib/wasm";

import type {User, PendingGame} from "$lib/types";


// the server that handles all moves and stuff through IPC, which is just stupid btw
// @ts-ignore
//import {spawn} from 'node:child_process';
//const zig_server = spawn('./src/zig/zig-out/bin/zig-server', []);

import {readFileSync} from "node:fs";

const wasm_file = readFileSync("static/lib/server.wasm");
export const zig_server = await initWasm(wasm_file, {});

export const piece_json = getPieceJson();

const prisma = new PrismaClient();

export async function getUserByName(username: string): Promise<User | null> {
	const user = await prisma.user.findUnique({
		where: {
			username: username,
		},
	});
	return user;
}

export async function getMovesSince(game_id: number, move_id: number) {
	const game = await prisma.game.findUnique({
		where: {
			id: game_id,
		},
	});
	if (!game) {
		return [];
	}
	return game.moves.slice(move_id);
}

export async function getGameById(game_id: number) {
	const game = await prisma.game.findUnique({
		where: {
			id: game_id,
		},
		include: {
			sente: true,
			gote: true,
		},
	});
	return game;
}

export async function getLoggedInUser(cookies: Cookies): Promise<User | null> {
	let session_id = cookies.get("sessionId");
	if (!session_id) return null;
	let session = await prisma.session.findUnique({
		where: {
			id: session_id,
		},
		include: {
			user: true,
		},
	});
	if (!session) return null;
	
	return session.user;
}

// https://developer.mozilla.org/en-US/docs/Glossary/Base64
function bytesToBase64(bytes: Uint8Array) {
	const binString = String.fromCodePoint(...bytes);
	return btoa(binString);
}

// https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
async function sha256(message: string) {
    const msgBuffer = new TextEncoder().encode(message);                    
    const hashBuffer = await crypto.subtle.digest('SHA-256', msgBuffer);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
    return hashHex;
}

async function createSalt() {
	return await sha256(Math.random().toString());
}

export async function createUser(
	username: string, password: string)
{
	let salt = await createSalt();
	const user = await prisma.user.create({
		data: {
			username: username,
			password: await passwordToHash(password, salt),
			salt: salt,
		},
	});
	return user;
}

export async function passwordToHash(password: string, salt: string) {
	return await sha256(password+salt);
}

export async function createSession(user: User) {
	let session_id;
	while (true) {
		const bytes = crypto.getRandomValues(new Uint8Array(32));
		session_id = bytesToBase64(bytes);

		let session = await prisma.session.findUnique({
			where: {
				id: session_id,
			}
		});
		if (!session) {
			break;
		}
	}
	return await prisma.session.create({
		data: {
			id: session_id,
			userId: user.id,
		},
	});
}

export async function deleteOldSession(cookies: Cookies) {
	let session_id = cookies.get("sessionId");
	if (!session_id) return;
	if (await prisma.session.findUnique({
		where: {
			id: session_id,
		}
	})) {
		await prisma.session.delete({
			where: {
				id: session_id,
			}
		});
	}
	cookies.delete("sessionId");
}

var pending_game_id_count = 0;
var pending_games: Array<PendingGame> = [];
export function createPendingGame(game: PendingGame) {
	game.id = pending_game_id_count++;
	pending_games.push(game);
}
export function getPendingGames() {
	return pending_games;
}
// TODO: remove and replace with getPendingGame() and deletePendingGame()
export function setPendingGames(new_games: Array<PendingGame>) {
	pending_games = new_games;
}
export async function createGameFromPendingGame(old: PendingGame, player2: User) {
	let player1 = await getUserByName(old.player);
	if (player1 == null) {
		throw error(400, {message: "something happened"});
	}
	let players = [];
	if (Math.random() > 0.5) {
		players = [player1, player2];
	} else {
		players = [player2, player1];
	}
	const secondsPerSide = 60*(old.minutesPerSide+old.hoursPerSide*60);
	return await prisma.game.create({
		data: {
			state: 0, // sente moves first
			moveCount: 0,
			isRated: old.gameIsRated,
			canStart: true,
			secondsPerSide: secondsPerSide,
			incrementInSeconds: old.incrementInSeconds,
			senteId: players[0].id,
			goteId: players[1].id,
			senteTime: secondsPerSide,
			goteTime: secondsPerSide,
		},
	});
}

export async function createChatMessage(game_id: number, user_id: number, str: string) {
	const message = await prisma.chatMessage.create({
		data: {
			userId: user_id,
			gameId: game_id,
			str: str,
		}
	});
	return message;
}

// move has to be validated already
export async function createMove(game_id: number, move: number) {
	// subtract le time
	let game = await getGameById(game_id);
	if (!game) throw error(404, "game doesn't exist");
	let newSenteTime = game.senteTime;
	let newGoteTime = game.goteTime;
	if (game.moves.length >= 2) {
		// @ts-ignore
		let subtract = (new Date() - game.lastMoveAt) / 1000
		if ((game.moves.length & 1) == 0) {
			newSenteTime -= subtract;
			newSenteTime += game.incrementInSeconds;
		} else {
			newGoteTime -= subtract;
			newGoteTime += game.incrementInSeconds;
		}
	}

	return await prisma.game.update({
		where: {
			id: game_id,
		},
		data: {
			lastMoveAt: new Date(),
			moves: {
				push: move,
			},
			senteTime: newSenteTime,
			goteTime: newGoteTime,
		}
	});
}

export async function getChatMessages(game_id: number, next_chat_id: number) {
	const data = await prisma.chatMessage.findMany({
		where: {
			gameId: game_id,
			id: {
				gte: next_chat_id,
			},
		},
		include: {
			user: true,
		},
	});
	return data;
}

export async function findGameYourIn(user_id: number) {
	const data = await prisma.game.findMany({
		where: {
			OR: [
				{
					senteId: user_id
				},
				{
					goteId: user_id
				}
			],
			canStart: true,
		},
	});
	// there should only be a single game!
	for (const game of data) {
		var can_actually_start = true;
		if (game.moveCount >= 2) {
			can_actually_start = false;
		}
		if (Date.now() - game.lastMoveAt.getTime() > 20*1000) {
			can_actually_start = false;
		}

		if (can_actually_start == false) {
			await prisma.game.update({
				where: {
					id: game.id,
				},
				data: {
					canStart: false,
				},
			});
		} else {
			return game;
		}
	}
	return null;
}
