export type User = {
	id: number,
	username: string,
	password: string, // hashed
	salt: string, // salt
	rating: number,
};

/*
export type Game = {
	id: number,
	state: number,
	firstMovesMade: boolean,
	isRated: boolean
	secondsPerSide: number,
	incrementInSeconds: number,
	sente: User,
	senteId: number,
	gote: User,
	goteId: number,
};
*/

export type PendingGame = {
	hoursPerSide: number,
	minutesPerSide: number,
	incrementInSeconds: number,
	gameIsRated: boolean,
	player: string,
	playerRating: number,
	id: number,
};

