
import type {Actions} from './$types';
import {redirect} from "@sveltejs/kit";

import {
	deleteOldSession,
} from "$lib/server/core";

export const actions: Actions = {
	default: async ({cookies}) => {
		await deleteOldSession(cookies);
		
		throw redirect(307, "/");
	}
}
