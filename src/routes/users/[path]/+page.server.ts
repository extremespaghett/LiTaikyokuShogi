import type {PageServerLoad} from "./$types";
import { error } from '@sveltejs/kit';

import {getUserByName} from "$lib/server/core";

export const load = (async ({url}) => {
	const username = url.pathname.split("/").pop();
	if (!username) {
		return; // unreachable
	}
	const user = await getUserByName(username);

	if (!user) {
		console.log("throwing");
		throw error(404, "user does not exist");
	}

	return {username: username}
}) satisfies PageServerLoad;
