import { 
	createPendingGame, getPendingGames, setPendingGames,
	getLoggedInUser, createGameFromPendingGame,
	findGameYourIn,
} from "$lib/server/core";

import type {PendingGame} from "$lib/types";

import type { RequestHandler } from '@sveltejs/kit';

export const GET: RequestHandler = async ({cookies}) => {
	let user = await getLoggedInUser(cookies);
	let yourGameId = null;
	if (user != null) {
		const your_game = await findGameYourIn(user.id);
		if (your_game) {
			yourGameId = your_game.id;
		}
	}
	let object = {
		pendingGames: getPendingGames(),
		yourGameId,
	};
	return new Response(JSON.stringify(object), {status: 200});
}

export const POST: RequestHandler = async ({cookies, request}) => {
	let data = await request.json();
	let user = await getLoggedInUser(cookies);
	if (!user) {
		return new Response(null, {status: 200});
	}
	var return_str = "";
	switch (data.type) {
	case "createGame":
		{
			// TODO: delete old pending games
			let pending_game: PendingGame = data.object;
			pending_game.player = user.username;
			pending_game.playerRating = user.rating;
			createPendingGame(pending_game);
		}
		break;
	case "cancelPendingGame":
		{
			let id = data.object.id;
			let pending_games = getPendingGames();

			let new_array: typeof pending_games = [];
			for (var game of pending_games) {
				if (game.id == id && user.username == game.player) {
					// delete
				} else {
					new_array.push(game);
				}
			}
			setPendingGames(new_array);
		}
		break;
	case "joinPendingGame":
		{
			let id = data.object.id;
			let pending_games = getPendingGames();
			// search for game
			// + delete the pending games of player1 and player2
			let no_goto_moment = true;
			for (let i = 0; i < pending_games.length; i++) {
				const game = pending_games[i];
				if (game.id == id && user.username != game.player) {
					const new_game = await createGameFromPendingGame(game, user);
					return_str = ""+new_game.id;
					pending_games.splice(i);
					no_goto_moment = false;
					break;
				}
			}
			if (no_goto_moment) {
				return_str = "failure";
			}
		}
		break;
	default: console.log("something went left", data);
	}
	return new Response(return_str, {status: 200});
}
