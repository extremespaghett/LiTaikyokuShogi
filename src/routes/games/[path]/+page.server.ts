import type {PageServerLoad} from "./$types";
import { error } from '@sveltejs/kit';

import {
	getLoggedInUser, getGameById
} from "$lib/server/core";

export const load = (async ({cookies, url}) => {
	let game_id_pre = url.pathname.split('/').pop();
	let game_id: number = 0;
	if (game_id_pre != undefined) {
		game_id = parseInt(game_id_pre);
	} else {
		throw error(404, "cant program for shit");
	}

	let game = await getGameById(game_id);
	if (!game) {
		throw error(404, "game does not exist");
	}

	let logged_in_user = await getLoggedInUser(cookies);
	let logged_in_username: string | null = null;
	if (logged_in_user) logged_in_username = logged_in_user.username;
	return {
		username: logged_in_username,
		sente: game.sente.username,
		gote: game.gote.username,
		lastMoveAt: game.lastMoveAt,
		senteTime: game.senteTime,
		goteTime: game.goteTime,
		incrementInSeconds: game.incrementInSeconds,
	}
}) satisfies PageServerLoad;
