import { 
	getLoggedInUser, createChatMessage, getChatMessages,
	getMovesSince, createMove,
	getGameById,
} from "$lib/server/core";

import type { RequestHandler } from '@sveltejs/kit';

import { error } from '@sveltejs/kit';

// TODO: delte this BS ASAP
export const GET: RequestHandler = async ({url}) => {
	let game_id_pre = url.pathname.split('/').pop();
	let game_id: number = 0;
	if (game_id_pre != undefined) {
		game_id = parseInt(game_id_pre);
	} else {
		throw error(404, "cant program for shit");
	}

	let object = {
	};
	return new Response(JSON.stringify(object), {status: 200});
}

export const POST: RequestHandler = async ({cookies, request, url}) => {
	let game_id_pre = url.pathname.split('/').pop();
	let game_id: number = 0;
	if (game_id_pre != undefined) {
		game_id = parseInt(game_id_pre);
	} else {
		throw error(404, "cant program for shit");
	}

	let data = await request.json();
	let user = await getLoggedInUser(cookies);
	let return_object;
	switch (data.type) {
	case "sendMsg":
		{
			if (!user) {
				throw error (400, "not logged in");
			}
			await createChatMessage(game_id, user.id, data.object.str);
		}
		break;
	case "sendMove":
		// TODO: validation
		{
			await createMove(game_id, data.object.move);
		}
		break;
	case "getDynamicData":
		{
			let game = await getGameById(game_id);
			if (!game) {
				throw error(404, "game doesn't exist");
			}
			let moves = await getMovesSince(game_id, data.object.next_move);
			return_object = {
				moves: moves,
				chats: (await getChatMessages(
					game_id, data.object.next_chat)).map((input) => {
						return {
							id: input.id,
							username: input.user.username,
							str: input.str,
						};
					}),
				lastMoveAt: game.lastMoveAt,
				senteTime: game.senteTime,
				goteTime: game.goteTime,
			};
		}
		break;
	default: console.log("someone sent wrong POST type");
	}
	return new Response(JSON.stringify(return_object), {status: 200});
}
