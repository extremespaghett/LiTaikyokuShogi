import type {PageServerLoad} from "./$types";
import type {Actions} from './$types';

import {
	getUserByName, createUser, createSession,
	getLoggedInUser, passwordToHash,
	deleteOldSession,
} from "$lib/server/core";

// @ts-ignore
export const load = (async ({cookies, url}) => {
	let theme_target = url.origin + "/themes/dracula.json";
	let theme = JSON.parse(await (await fetch(theme_target)).text());
	let logged_in_user = await getLoggedInUser(cookies);
	let logged_in_username: string | null = null;
	if (logged_in_user) logged_in_username = logged_in_user.username;
	return {theme: theme, username: logged_in_username}
}) satisfies PageServerLoad;

export const actions: Actions = {
	default: async ({cookies, request}) => {
		// first delete old session
		await deleteOldSession(cookies);

		// then try to see if a login or register is possible
		let form_data = await request.formData();
		let username = form_data.get("username")?.toString();
		let password = form_data.get("password")?.toString();
		if (!username) return; // unreachable
		if (!password) return; // unreachable

		let user = await getUserByName(username);

		if (form_data.get("login")) {
			if (user) {
				const hash_password = await passwordToHash(password, user.salt);
				if (user.password != hash_password) {
					user = null;
				}
			}
		} else if (form_data.get("register")) {
			if (!user) {
				// can fail if username already taken
				user = await createUser(username, password);
			} else {
				user = null;
			}
		}

		// create session
		if (user) {
			let expire_date = new Date();
			expire_date.setFullYear(expire_date.getFullYear()+1);
			let session = await createSession(user);
			cookies.set("sessionId", session.id, {
				expires: expire_date,
			});
		}
	}
};
