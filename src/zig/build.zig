const std = @import("std");

pub fn addClientWasm(b: *std.Build) void {
    const lib = b.addSharedLibrary(.{
        .name = "main",

        .root_source_file = .{ .path = "src/client.zig" },

        .target = .{
            .cpu_arch = .wasm32,
            .os_tag = .freestanding,
            .abi = .musl,
        },

        .optimize = .ReleaseSmall,
    });

    lib.global_base = 6560;
    lib.rdynamic = true;
    //lib.import_memory = true;
    lib.stack_size = std.wasm.page_size;

    lib.initial_memory = std.wasm.page_size * 0; // 128KiB
    //lib.max_memory = std.wasm.page_size * 128; // 16 MiB

    b.installArtifact(lib);
}
pub fn addServerWasm(b: *std.Build) void {
    const lib = b.addSharedLibrary(.{
        .name = "server",

        .root_source_file = .{ .path = "src/server.zig" },

        .target = .{
            .cpu_arch = .wasm32,
            .os_tag = .freestanding,
            .abi = .musl,
        },

        .optimize = .ReleaseSmall,
    });

    lib.global_base = 6560;
    lib.rdynamic = true;
    //lib.import_memory = true;
    lib.stack_size = std.wasm.page_size;

    lib.initial_memory = std.wasm.page_size * 0; // 128KiB
    //lib.max_memory = std.wasm.page_size * 128; // 16 MiB

    b.installArtifact(lib);
}

pub fn build(b: *std.Build) void {
    addClientWasm(b);
    addServerWasm(b);
}
