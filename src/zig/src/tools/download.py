import html_to_json
import os
import json

html_string = os.popen("curl https://en.wikipedia.org/wiki/Taikyoku_shogi").read()
output_json = html_to_json.convert(html_string)

print(output_json);
f = open("page.json", "w")
json.dump(output_json, f);

f.close()
