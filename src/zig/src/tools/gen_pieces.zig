// generates pieces (moves, names, kanji, etc.) from pieces of the wikipedia article
//
// the promoted pieces are also counted as pieces to simplify everything

// TODO: check if DOG (with 1 kanji) is being generated

const std = @import("std");

const whole_page_json = @embedFile("page.json");
const piece_list = @embedFile("piece_list.json");
const letters_file = @embedFile("letters.txt");
const board_layout_file = @embedFile("board_layout.json");

const Step1Piece = struct {
    name: []const u8,
    kanji: []const u8,
    letters: []const u8, // promoted pieces simply color the letters/kanji red
    promotion: []const u8,
    promotion_kanji: []const u8,
    only_exists_promoted: bool,
};

pub const no_promotion_constant: u16 = 65535;
pub const FinalPiece = struct {
    name: []const u8,
    kanji: []const u8,
    letters: []const u8,
    only_exists_promoted: bool,
    promotion: u16,
    // (y * 3) + x, how many steps
    // the pieces that can't be defined like this are hard-coded
    // (minority)
    directions: ?[9]u8 = null,
};

fn step1(allocator: std.mem.Allocator) !std.ArrayList(Step1Piece) {
    var list = std.ArrayList(Step1Piece).init(allocator);
    var parsed = try std.json.parseFromSlice(std.json.Value, allocator, piece_list, .{});
    //defer parsed.deinit(); who needs this bs?
    const tbody = parsed.value.object.get("tbody") orelse @panic("wrong json");
    const tr = tbody.object.get("tr") orelse @panic("wrong json");
    // begins at 1
    for (tr.array.items, 0..) |value, i| {
        if (i == 0) { // header
            continue;
        }
        const td = value.object.get("td") orelse @panic("wrong json");
        const name_value = td.array.items[0];

        // parse piece
        var piece_name: []u8 = undefined;
        switch (name_value) {
            .string => |str| {
                // cant be automated (or can be but thats stupid)
                if (std.mem.eql(u8, str, "Howling dog (left and right)")) {
                    try list.append(.{
                        .name = "Left howling dog",
                        .kanji = "𠵇犬",
                        .letters = "HD",
                        .promotion = "Left dog",
                        .promotion_kanji = "左犬",
                        .only_exists_promoted = false,
                    });
                    try list.append(.{
                        .name = "Right howling dog",
                        .kanji = "𠵇犬",
                        .letters = "HD",
                        .promotion = "Right dog",
                        .promotion_kanji = "右犬",
                        .only_exists_promoted = false,
                    });
                    continue;
                }
                piece_name = @constCast(str);
            },
            else => |obj| {
                const a = obj.object.get("a") orelse @panic("wrong json");
                const a_text = a.object.get("#text") orelse @panic("wrong json");
                const text = obj.object.get("#text");
                if (text) |text_value| {
                    piece_name = try std.fmt.allocPrint(allocator, "{s} {s}", .{ text_value.string, a_text.string });
                } else {
                    piece_name = try std.fmt.allocPrint(allocator, "{s}", .{a_text.string});
                }
            },
        }
        // parse promoted piece
        const promoted_piece_value = td.array.items[3];
        const promotion = switch (promoted_piece_value) {
            .string => |str_pre| blk: {
                if (std.mem.eql(u8, str_pre, "—")) {
                    break :blk "—";
                }
                var is_new = false;
                if (str_pre[0] == '*') {
                    is_new = true;
                    break :blk str_pre[1..];
                } else {
                    break :blk str_pre;
                }
            },
            else => |obj| blk: {
                // edgecase
                if (std.mem.eql(u8, piece_name, "Woodland demon")) {
                    break :blk "Right phoenix";
                }
                const a = obj.object.get("a") orelse @panic("wrong json");
                const a_text = a.object.get("#text") orelse @panic("wrong json");
                const text_pre = obj.object.get("#text");

                if (text_pre) |text_value| {
                    const str = str_blk: {
                        if (text_value.string[0] == '*') {
                            break :str_blk text_value.string[1..];
                        } else {
                            break :str_blk text_value.string;
                        }
                    };
                    // edgecase
                    if (std.mem.eql(u8, piece_name, "Old monkey")) {
                        break :blk "Mountain witch";
                    } else {
                        break :blk try std.fmt.allocPrint(allocator, "{s} {s}", .{ str, a_text.string });
                    }
                } else {
                    break :blk try std.fmt.allocPrint(allocator, "{s}", .{a_text.string});
                }
            },
        };
        const promotion_kanji = blk: {
            if (std.mem.eql(u8, promotion, "Bird of paradise")) {
                break :blk "TODO1"; // use special svgs here!
            } else if (std.mem.eql(u8, promotion, "Wizard stork")) {
                break :blk "TODO2"; // same thing
            } else {
                break :blk td.array.items[4].string;
            }
        };
        try list.append(.{
            .name = piece_name,
            .kanji = td.array.items[1].string,
            .letters = undefined,
            .promotion = promotion,
            .promotion_kanji = promotion_kanji,
            .only_exists_promoted = false,
        });
    }
    return list;
}

fn search_letters(name: []const u8) []const u8 {
    // edgecase
    if (std.mem.eql(u8, name, "Left howling dog")) {
        return "HD";
    }
    if (std.mem.eql(u8, name, "Right howling dog")) {
        return "HD";
    }
    var i: u32 = 0;
    while (i < letters_file.len) : (i += 1) {
        const name_start_index = i + 7;
        const letters_start_index = i;
        i = name_start_index;
        // find newline index
        while (true) {
            if (!(i < letters_file.len)) {
                unreachable;
            }
            if (letters_file[i] == '\n') {
                break;
            }
            i += 1;
        }
        const curr_name = letters_file[name_start_index..i];
        if (std.ascii.eqlIgnoreCase(curr_name, name)) {
            if (letters_file[letters_start_index + 1] == ' ') {
                return letters_file[letters_start_index..(letters_start_index + 1)];
            } else {
                return letters_file[letters_start_index..(letters_start_index + 2)];
            }
        }
    }
    unreachable;
}

fn step2(list: *std.ArrayList(Step1Piece)) !void {
    for (list.items) |*element| {
        element.letters = search_letters(element.name);
    }
}

fn search(name: []const u8, list: std.ArrayList(Step1Piece)) ?usize {
    for (list.items, 0..) |element, i| {
        if (std.mem.eql(u8, name, element.name)) {
            return i;
        }
    }
    return null;
}

// makes promotion pieces into own pieces
fn step3(list: *std.ArrayList(Step1Piece)) !void {
    var i: u32 = 0;
    while (i < list.items.len) : (i += 1) {
        const piece = list.items[i];
        if (std.mem.eql(u8, piece.promotion, "—")) {
            continue;
        }
        const optional_index = search(piece.promotion, list.*);
        if (optional_index == null) {
            try list.append(.{
                .name = piece.promotion,
                .kanji = piece.promotion_kanji,
                .letters = piece.letters,
                .promotion = "—",
                .promotion_kanji = "",
                .only_exists_promoted = true,
            });
        }
    }
}

// from promotion to index
fn step4(allocator: std.mem.Allocator, pre_list: std.ArrayList(Step1Piece)) !std.ArrayList(FinalPiece) {
    var list = std.ArrayList(FinalPiece).init(allocator);
    for (pre_list.items) |piece| {
        var promotion_index: u16 = undefined;
        if (std.mem.eql(u8, piece.promotion, "—")) {
            promotion_index = @intCast(no_promotion_constant);
        } else {
            promotion_index = @intCast(search(piece.promotion, pre_list) orelse unreachable);
        }
        try list.append(.{
            .name = piece.name,
            .kanji = piece.kanji,
            .letters = piece.letters,
            .promotion = promotion_index,
            .only_exists_promoted = piece.only_exists_promoted,
        });
    }
    return list;
}

fn printSpaces(len: u32) void {
    for (0..len) |_| {
        std.debug.print(" ", .{});
    }
}

fn printJSON(input: anytype, recursion: u32) void {
    if (recursion > 4) {
        return;
    }
    printSpaces(recursion * 2);
    switch (input) {
        .string => |str| {
            std.debug.print("JSON(str): {s}", .{str});
            if (recursion == 0) {
                std.debug.print("  <---\n", .{});
            } else {
                std.debug.print("\n", .{});
            }
        },
        .object => |obj| {
            std.debug.print("JSON(object):", .{});
            if (recursion == 0) {
                std.debug.print("  <---\n", .{});
            } else {
                std.debug.print("\n", .{});
            }
            for (obj.keys()) |keys| {
                printSpaces(recursion * 2 + 2);
                std.debug.print("{s}\n", .{keys});
            }
        },
        .array => |arr| {
            std.debug.print("JSON(array):", .{});
            if (recursion == 0) {
                std.debug.print("  <---\n", .{});
            } else {
                std.debug.print("\n", .{});
            }
            for (arr.items) |element| {
                printJSON(element, recursion + 1);
            }
        },
        else => unreachable,
    }
}

// get piece tables
fn step5(allocator: std.mem.Allocator, list: *std.ArrayList(FinalPiece)) !void {
    var parsed = try std.json.parseFromSlice(std.json.Value, allocator, whole_page_json, .{});
    const html = parsed.value.object.get("html") orelse unreachable;
    const search_ret = (searchJSON(&html, "mw-content-text", allocator) orelse unreachable);
    const parent1 = search_ret.map.get(search_ret.value) orelse unreachable;
    const main_div = search_ret.map.get(parent1) orelse unreachable;
    const inner_div = main_div.object.get("div") orelse unreachable;
    //printJSON(main_div.*, 0);
    const top10kod2023 = (inner_div.array.items[0].object.get("meta") orelse unreachable).array.items[0];
    //printJSON(top10kod2023, 0);

    // problem: different amount of h5 and table
    // solution: find Pawn (歩兵) str and calculate index in h5
    const h5s = top10kod2023.object.get("h5") orelse unreachable;
    const tables = top10kod2023.object.get("table") orelse unreachable;
    const divs = top10kod2023.object.get("div") orelse unreachable;

    const SpecialCase = struct {
        name: []const u8,
        table_is_div: bool = false,
        different_names: ?[]const []const u8 = null,
    };
    const special_cases = [_]SpecialCase{
        .{
            .name = "Violent bear (猛熊)",
            .table_is_div = true,
        },
        .{
            .name = "Treacherous fox (隠狐)",
            .table_is_div = true,
        },
        .{
            .name = "Divine sparrow (神雀)",
            .table_is_div = true,
        },
        .{
            .name = "Earth dragon (地龍)",
            .table_is_div = true,
        },
        .{
            .name = "Free demon (奔鬼)",
            .table_is_div = true,
        },
        .{
            .name = "Mountain falcon (山鷹)",
            .table_is_div = true,
        },
        .{
            .name = "Capricorn (摩羯)",
            .table_is_div = true,
        },
        .{
            .name = "Lion (獅子)",
            .table_is_div = true,
        },
        .{
            .name = "Furious fiend (奮迅)",
            .table_is_div = true,
        },
        .{
            .name = "Buddhist spirit (法性)",
            .table_is_div = true,
        },
        .{
            .name = "Lion hawk (獅鷹)",
            .table_is_div = true,
        },
        // Different pieces start
        .{
            .name = "Running boar (走猪),  Running pup (走狗),  running serpent (走蛇), Earth chariot (土車) and Vertical-mover (竪行).",
            .different_names = &[_][]const u8{ "Running boar", "Running pup", "running serpent", "earth chariot", "vertical mover" },
        },
        .{
            .name = "Howling dog (left and right) (𐵇犬)",
            .different_names = &[_][]const u8{ "left howling dog", "right howling dog" },
        },
        .{
            .name = "Lion-dog (狛犬)",
            .different_names = &[_][]const u8{"Lion dog"},
        },
        .{
            .name = "Side-mover (横行)",
            .different_names = &[_][]const u8{"Side mover"},
        },
    };

    // giving up on automating pieces above knight
    const hard_code_start = "Knight (桂馬)";

    var table_index: u32 = 2;
    var div_index: u32 = 2;
    // find pawn (h5)
    var pawn_found = false;
    for (h5s.array.items) |element| {
        const span = element.object.get("span") orelse continue;
        const value_obj = span.array.items[1];
        const value = (value_obj.object.get("_value") orelse if (pawn_found) unreachable else continue).string;

        if (std.mem.eql(u8, value, hard_code_start)) {
            break;
        }

        if (std.mem.eql(u8, value, "Pawn (歩兵)")) {
            pawn_found = true;
        }
        if (pawn_found) blk: {
            var curr_pieces: [8]u32 = undefined;
            var curr_piece_count: u32 = 0;
            for (special_cases) |case| special: {
                if (std.mem.eql(u8, value, case.name)) {
                    if (case.different_names) |different_names| {
                        for (different_names) |name| {
                            found: {
                                for (list.items, 0..) |piece, i| {
                                    if (std.ascii.eqlIgnoreCase(name, piece.name)) {
                                        curr_pieces[curr_piece_count] = @intCast(i);
                                        curr_piece_count += 1;
                                        break :found;
                                    }
                                }
                                unreachable;
                            }
                        }
                        break :special;
                    }
                    if (div_index >= divs.array.items.len) unreachable;

                    div_index += 1;
                    break :blk;
                }
            }
            // is table case
            if (table_index >= tables.array.items.len) unreachable;

            if (curr_piece_count == 0) gen_curr_piece: {
                // edge case
                if (std.ascii.eqlIgnoreCase(value, "Heavenly tetrarch king (四天王)")) {
                    for (list.items, 0..) |piece, i| {
                        if (std.mem.eql(u8, piece.name, "Heavenly tetrarch king")) {
                            curr_pieces[curr_piece_count] = @intCast(i);
                            curr_piece_count += 1;
                            break :gen_curr_piece;
                        }
                    }
                    unreachable;
                }
                var start: usize = 0;
                while (start < value.len) : (start += 1) {
                    for (list.items, 0..) |piece, i| {
                        if (start + piece.name.len > value.len) {
                            continue;
                        }
                        const cmp = value[start .. start + piece.name.len];
                        if (std.ascii.eqlIgnoreCase(piece.name, cmp)) {
                            curr_pieces[curr_piece_count] = @intCast(i);
                            curr_piece_count += 1;

                            start += piece.name.len - 1;
                            break;
                        }
                    }
                }
            }
            var directions = [9]u8{ 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            const table = tables.array.items[table_index];
            const tbody = table.object.get("tbody") orelse unreachable;
            const tr = tbody.array.items[0].object.get("tr") orelse unreachable;
            const height = tr.array.items.len;
            const width = (tr.array.items[0].object.get("td") orelse unreachable).array.items.len;
            const center_x = width / 2;
            const center_y = height / 2;
            //printJSON(tr, 0);
            //std.debug.print("({}, {}): ({}, {})\n", .{ width, height, center_x, center_y });
            for (0..height) |y| {
                for (0..width) |x| {
                    const row = tr.array.items[y].object.get("td") orelse unreachable;
                    const square = row.array.items[x];
                    if (square.object.get("_value")) |square_val| {
                        const diff_x: i32 = @as(i32, @intCast(x)) - @as(i32, @intCast(center_x));
                        const diff_y: i32 = @as(i32, @intCast(y)) - @as(i32, @intCast(center_y));
                        var x_pos: u16 = 1;
                        var y_pos: u16 = 1;
                        if (diff_x > 0) {
                            x_pos = 2;
                        } else if (diff_x < 0) {
                            x_pos = 0;
                        }
                        if (diff_y > 0) {
                            y_pos = 2;
                        } else if (diff_y < 0) {
                            y_pos = 0;
                        }
                        const dir = 3 * y_pos + x_pos;
                        const diff: u8 = @intCast(try std.math.absInt(if (diff_y != 0) diff_y else diff_x));
                        if (std.mem.eql(u8, square_val.string, "○")) {
                            directions[dir] = @max(diff, directions[dir]);
                        } else if (std.mem.eql(u8, square_val.string, "│")) {
                            directions[dir] = 255;
                        } else if (std.mem.eql(u8, square_val.string, "─")) {
                            directions[dir] = 255;
                        } else if (std.mem.eql(u8, square_val.string, "╱")) {
                            directions[dir] = 255;
                        } else if (std.mem.eql(u8, square_val.string, "╲")) {
                            directions[dir] = 255;
                        } else if (std.mem.eql(u8, square_val.string, "☖")) {} else unreachable;
                    }
                }
            }
            if (false) {
                std.debug.print("{s}: ", .{value});
                for (curr_pieces[0..curr_piece_count]) |piece| {
                    std.debug.print("{s}, ", .{list.items[piece].name});
                }
                std.debug.print("\n", .{});
            }
            for (curr_pieces[0..curr_piece_count]) |piece| {
                list.items[piece].directions = directions;
            }

            table_index += 1;
        }
    }
}

const SearchJSONMapType = std.AutoHashMap(*const std.json.Value, *const std.json.Value); // child => parent
const SearchJSONReturn = struct {
    map: *SearchJSONMapType,
    value: *const std.json.Value,
};
fn searchJSON(json_in: *const std.json.Value, str: []const u8, map_or_allocator: anytype) ?SearchJSONReturn {
    var parent_map: *SearchJSONMapType = switch (@TypeOf(map_or_allocator)) {
        std.mem.Allocator => blk: {
            var mem = map_or_allocator.create(SearchJSONMapType) catch unreachable;
            mem.* = SearchJSONMapType.init(map_or_allocator);
            break :blk mem;
        },
        *SearchJSONMapType => map_or_allocator,
        else => unreachable,
    };
    switch (json_in.*) {
        .string => |curr_str| {
            if (std.mem.eql(u8, str, curr_str)) {
                return .{ .map = parent_map, .value = json_in };
            }
        },
        .array => |arr| {
            for (arr.items) |*element| {
                parent_map.put(element, json_in) catch unreachable;
                if (searchJSON(element, str, parent_map)) |val| {
                    return val;
                }
            }
        },
        .object => |obj| {
            for (obj.values()) |*element| {
                parent_map.put(element, json_in) catch unreachable;
                if (searchJSON(element, str, parent_map)) |val| {
                    return val;
                }
            }
        },
        else => unreachable,
    }
    return null;
}

fn genBoardLayout(allocator: std.mem.Allocator, list: std.ArrayList(FinalPiece)) ![]u16 {
    const width = 36;
    const height = 13;
    var parsed = try std.json.parseFromSlice(std.json.Value, allocator, board_layout_file, .{});
    const tbody = parsed.value.object.get("tbody") orelse unreachable;
    const tr = tbody.object.get("tr") orelse unreachable;

    var array = try allocator.alloc(u16, width * height);
    var index: usize = 0;
    for (0..height) |y| {
        const row = tr.array.items[y].object.get("td") orelse unreachable;
        for (0..width) |x| {
            const square = row.array.items[x].string;
            if (square.len == 0) {
                array[index] = no_promotion_constant;
            } else blk: {
                // HD is an edge case
                if (std.mem.eql(u8, "HD", square)) {
                    if (x > (width / 2)) {
                        array[index] = 106;
                    } else {
                        array[index] = 107;
                    }
                    break :blk;
                }
                for (list.items, 0..) |piece, i| {
                    if (std.mem.eql(u8, piece.letters, square)) {
                        array[index] = @intCast(i);
                        break :blk;
                    }
                }
                unreachable;
            }

            //std.debug.print("{}, {}: {}\n", .{ x, y, array[index] });

            index += 1;
        }
    }
    return array;
}

pub fn main() !void {
    // stupid
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    //defer _ = gpa.deinit(); HEE HEE HEE HAW

    const stdout = std.io.getStdOut().writer();
    var pre_pieces = try step1(allocator);
    try step2(&pre_pieces);
    try step3(&pre_pieces);
    var pieces = try step4(allocator, pre_pieces);
    try step5(allocator, &pieces);
    if (false) {
        for (pieces.items, 0..) |piece, index| {
            try stdout.print("{}|{s}: {s}, ({s}) => {}. ({any})\n", .{ index, piece.name, piece.kanji, piece.letters, piece.promotion, piece.directions });
        }
    }
    const layout = try genBoardLayout(allocator, pieces);

    // finally write json output
    const pieces_file = try std.fs.cwd().createFile(
        "piece_output.json",
        .{ .truncate = true },
    );
    defer pieces_file.close();
    try std.json.stringify(pieces.items, .{}, pieces_file.writer());
    const layout_file = try std.fs.cwd().createFile(
        "layout_output.json",
        .{ .truncate = true },
    );
    defer layout_file.close();
    try std.json.stringify(layout, .{}, layout_file.writer());

    {
        const txt_pieces_file = try std.fs.cwd().createFile(
            "piece_output.txt",
            .{ .truncate = true },
        );
        for (pieces.items) |piece| {
            const ptr: [*]const u8 = @ptrCast(&piece.promotion);
            const slice: []const u8 = ptr[0..2];
            _ = try txt_pieces_file.writer().print("{s}\n{s}\n{s}\n{s}\n{?s}\n", .{ piece.name, piece.kanji, piece.letters, slice, piece.directions });
        }
    }
    {
        const bin_layout_file = try std.fs.cwd().createFile(
            "layout_output.bin",
            .{ .truncate = true },
        );
        const new_ptr: [*]u8 = @ptrCast(layout.ptr);
        var new_slice: []u8 = undefined;
        new_slice.ptr = new_ptr;
        new_slice.len = layout.len * 2;
        _ = try bin_layout_file.writer().write(new_slice);
    }
}
