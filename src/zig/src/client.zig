const std = @import("std");

const common = @import("common.zig");
const log = common.log;

const Game = @import("game.zig").Game;
const Move = @import("game.zig").Move;
const legal_move_buffer_size = @import("game.zig").legal_move_buffer_size;

var game: Game = undefined;

export fn main() usize {
    var allocator = common.createAllocator();
    _ = allocator;

    log("hello from wasm!", .{});

    //log("test: {any}", .{@import("game.zig").piece_descriptions[299].directions});
    return 0;
}

export fn setGame(move_ptr: usize, move_len: usize) usize {
    game.initStartPosition();

    makeMoves(move_ptr, move_len);

    return @intFromPtr(&game.squares);
}

export fn makeMoves(move_ptr: usize, move_len: usize) void {
    if (move_len == 0) {
        return;
    }
    var buffer: [*]Move = @ptrFromInt(move_ptr);
    var slice: []Move = buffer[0..move_len];
    game.makeMoves(slice);
}

export fn calculateAllLegalMoves(buffer_ptr: usize, gote: usize) usize {
    var buffer: [*]Move = @ptrFromInt(buffer_ptr);
    var slice: []Move = buffer[0..legal_move_buffer_size];
    return game.calculateAllLegalMoves(&slice, gote > 0);
}

export fn calculatePieceLegalMoves(buffer_ptr: usize, square: usize) usize {
    var buffer: [*]Move = @ptrFromInt(buffer_ptr);
    var slice: []Move = buffer[0..legal_move_buffer_size];
    return game.calculatePieceLegalMoves(square, &slice);
}

export fn allocateMoveBuffer(in_len: usize) usize {
    var len: usize = in_len;
    if (in_len == 0) {
        len = legal_move_buffer_size;
    }
    const allocator = common.allocator;
    const buffer = allocator.alloc(Move, len) catch |err| return common.ec(err);
    return @intFromPtr(buffer.ptr);
}

export fn freeMoveBuffer(ptr: usize, in_len: usize) void {
    var len: usize = in_len;
    if (in_len == 0) {
        len = legal_move_buffer_size;
    }
    const allocator = common.allocator;
    var buffer: [*]Move = @ptrFromInt(ptr);
    allocator.free(buffer[0..len]);
}

export fn allocateMem(bytes: usize) usize {
    var allocator = common.allocator;
    const data = allocator.alloc(u8, bytes) catch |err| return common.ec(err);
    return @intFromPtr(data.ptr);
}

export fn freeMem(ptr: usize, bytes: usize) void {
    var allocator = common.allocator;
    var new_ptr: [*]u8 = @ptrFromInt(ptr);
    allocator.free(new_ptr[0..bytes]);
}

export fn getPieceJsonPtr() usize {
    return @intFromPtr(common.piece_json.ptr);
}

export fn getPieceJsonLen() usize {
    return common.piece_json.len;
}
