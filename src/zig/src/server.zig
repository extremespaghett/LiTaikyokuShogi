const std = @import("std");

const common = @import("common.zig");
const log = common.log;

const Game = @import("game.zig").Game;

export fn main() usize {
    var allocator = common.createAllocator();
    _ = allocator;

    log("hello", .{});
    return 0;
}

extern fn getMovesJS(id: usize, ptr: usize, len: usize) void;

export fn getPieceJsonPtr() usize {
    return @intFromPtr(common.piece_json.ptr);
}

export fn getPieceJsonLen() usize {
    return common.piece_json.len;
}
