// common wasm code shared between server and client

// Note:
// LLVM wasm backend is not fancy and does not support shit as globals
// https://lists.llvm.org/pipermail/llvm-dev/2017-February/110211.html

const std = @import("std");

pub const piece_json = @embedFile("tools/piece_output.json");
pub const layout_json = @embedFile("tools/layout_output.json");

// used since you can comptime it
pub const piece_txt = @embedFile("tools/piece_output.txt");
pub const layout_bin = @embedFile("tools/layout_output.bin");

extern fn consoleLogJS(ptr: usize, size: usize) void;
extern fn notifyResizeJS() void;

pub fn log(comptime fmt: []const u8, args: anytype) void {
    const str = std.fmt.allocPrint(allocator, fmt, args) catch @panic("OutOfMemory");
    consoleLogJS(@intFromPtr(str.ptr), str.len);
}

pub var allocator: std.mem.Allocator = undefined;
var gpa: std.heap.GeneralPurposeAllocator(.{}) = undefined;

pub fn createAllocator() std.mem.Allocator {
    gpa = @TypeOf(gpa){ .backing_allocator = backing_allocator };
    allocator = gpa.allocator();
    return allocator;
}

pub const backing_allocator = std.mem.Allocator{
    .ptr = undefined,
    .vtable = &std.mem.Allocator.VTable{
        .alloc = allocAllocatorOverride,
        .free = freeAllocatorOverride,
        .resize = resizeAllocatorOverride,
    },
};
fn allocAllocatorOverride(ctx: *anyopaque, len: usize, ptr_align: u8, ret_addr: usize) ?[*]u8 {
    const result = std.heap.WasmPageAllocator.vtable.alloc(ctx, len, ptr_align, ret_addr);
    notifyResizeJS();
    return result;
}
fn freeAllocatorOverride(ctx: *anyopaque, buf: []u8, buf_align: u8, ret_addr: usize) void {
    const result = std.heap.WasmPageAllocator.vtable.free(ctx, buf, buf_align, ret_addr);
    notifyResizeJS();
    return result;
}
fn resizeAllocatorOverride(ctx: *anyopaque, buf: []u8, buf_align: u8, new_len: usize, ret_addr: usize) bool {
    const result = std.heap.WasmPageAllocator.vtable.resize(ctx, buf, buf_align, new_len, ret_addr);
    notifyResizeJS();
    return result;
}

pub fn ec(err: anyerror) usize {
    const oom: usize = @bitCast(@as(isize, -1));
    const js_error: usize = @bitCast(@as(isize, -2));
    const other_error: usize = @bitCast(@as(isize, -2));

    log("returning error: {}", .{err});

    return switch (err) {
        error.OutOfMemory => oom,
        error.js_logic_error => js_error,
        else => other_error,
    };
}
