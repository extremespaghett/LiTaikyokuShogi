const std = @import("std");
const common = @import("common.zig");
const log = common.log;

const piece_type_count = 301;

pub const Piece = u16;
pub const not_a_piece: Piece = 65535;
pub const color_mask: Piece = 1 << 15; // | with piece to get gote
pub const promotion_mask: Piece = 1 << 14;
pub const piece_mask: Piece = 0b11111111111111;
pub const description_no_promotion: Piece = 65535;

pub const Move = u32;
// stept into another square first
pub const is_igui_mask: Move = 0b1 << 24;
// 0: -1
// 1: 0
// 2: +1
pub const igui_x_mask: Move = 0b11 << 25;
pub const igui_y_mask: Move = 0b11 << 27;

// TODO: use bitfields with genLineFrom
// and genLine
// 1 bit = 1 step in line

// use 1 fixed legal moves array instead of allocating every time
// TODO: how many moves are possible in a position?
pub const legal_move_buffer_size = 8000;

pub const PieceDescription = struct {
    promotion: Piece,
    directions: [9]u8,
};
pub const piece_descriptions = blk: {
    @setEvalBranchQuota(10000);
    var descriptions: [301]@import("game.zig").PieceDescription = undefined;
    var i: u32 = 0;
    var piece_i: u32 = 0;
    inner: {
        while (true) {
            while (true) {
                if (piece_i == 301) {
                    break :inner;
                }
                while (true) : (i += 1) {
                    if (common.piece_txt[i] == '\n') {
                        i += 1;
                        break;
                    }
                }
                while (true) : (i += 1) {
                    if (common.piece_txt[i] == '\n') {
                        i += 1;
                        break;
                    }
                }
                while (true) : (i += 1) {
                    if (common.piece_txt[i] == '\n') {
                        i += 1;
                        break;
                    }
                }
                var promotion: u16 = @intCast(common.piece_txt[i]);
                promotion += @as(u16, @intCast(common.piece_txt[i + 1])) << 8;
                i += 3;
                const direction_start = i;
                i += 10;
                descriptions[piece_i] = .{
                    .promotion = promotion,
                    .directions = common.piece_txt[direction_start..(direction_start + 9)].*,
                };
                piece_i += 1;
            }
        }
    }
    break :blk descriptions;
};

pub const Game = struct {
    squares: [36 * 36]Piece,

    const Self = @This();

    pub fn initStartPosition(self: *Self) void {
        @setEvalBranchQuota(2000);
        const og_board = comptime blk: {
            var board: [36 * 36]u16 = undefined;
            for (&board) |*square| {
                square.* = not_a_piece;
            }
            var new_ptr: [*]const u16 = @alignCast(@ptrCast(common.layout_bin.ptr));
            var new_slice: []const u16 = undefined;
            new_slice.ptr = new_ptr;
            new_slice.len = common.layout_bin.len >> 1;

            @memcpy(board[(23 * 36)..], new_slice);
            for (0..13) |y| {
                for (0..36) |x| {
                    const dst = y * 36 + x;
                    const src = (35 - y) * 36 + x;
                    board[dst] = board[src] | color_mask;
                }
            }

            break :blk board;
        };
        @memcpy(&self.squares, &og_board);
    }

    pub fn makeMoves(self: *Self, moves: []Move) void {
        for (moves) |move| {
            // TODO: special cases
            const src: u16 = @intCast(move & 0b111111111111);
            const dst: u16 = @intCast((move >> 12) & 0b111111111111);
            const piece = self.squares[src];
            var promotes = false;
            if ((piece & promotion_mask) == 0) {
                var dst_y = dst / 36;
                if ((piece & color_mask) > 0) {
                    if (dst_y >= (25)) {
                        promotes = true;
                    }
                } else {
                    if (dst_y <= 10) {
                        promotes = true;
                    }
                }
            }
            var src_piece = self.squares[src];
            var dst_piece = src_piece;
            var src_color_mask = src_piece & color_mask;
            if (promotes) {
                const description = piece_descriptions[src_piece & piece_mask];
                if (description.promotion != description_no_promotion) {
                    dst_piece = description.promotion | promotion_mask | src_color_mask;
                }
            }
            if ((move & is_igui_mask) > 0) {
                const igui_x = ((move >> 25) & 0b11) - 1;
                const igui_y = ((move >> 27) & 0b11) - 1;
                const midx = (dst % 36) + igui_x;
                const midy = (dst / 36) + igui_y;
                const mid = midy * 36 + midx;
                self.squares[mid] = not_a_piece;
            }
            var coolguys_list = [_]u8{ // Piece: diagonal<<1 | orthogonal
                2,   3,
                84,  2,
                51,  1,
                50,  2,
                82,  2,
                229, 1,
            };
            for (0..(coolguys_list.len / 2)) |i| {
                if (coolguys_list[i << 1] != src_piece & piece_mask) {
                    continue;
                }
                var x1: isize = src % 36;
                var y1: isize = src / 36;
                var x2: isize = dst % 36;
                var y2: isize = dst / 36;
                // diagonal
                if (std.math.absInt(x2 - x1) catch unreachable == std.math.absInt(y2 - y1) catch unreachable) {
                    if (coolguys_list[(i << 1) | 1] & 2 == 0) {
                        continue;
                    }
                }
                if (x2 - x1 == 0 or y2 - y1 == 0) {
                    if (coolguys_list[(i << 1) | 1] & 1 == 0) {
                        continue;
                    }
                }
                var incx = std.math.sign(x2 - x1);
                var incy = std.math.sign(y2 - y1);
                var currx = x1;
                var curry = y1;
                while (true) {
                    if (currx == x2 and curry == y2) {
                        break;
                    }
                    var curr = curry * 36 + currx;
                    self.squares[@as(usize, @bitCast(curr))] = not_a_piece;
                    currx += incx;
                    curry += incy;
                }
            }
            self.squares[src] = not_a_piece;
            self.squares[dst] = dst_piece;
        }
    }

    // TODO: rename to genSquare
    fn checkSquare(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, buffer: *[]Move) void {
        const src_x = src % 36;
        const src_y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        const dst_x = src_x +% @as(usize, @bitCast(x_inc));
        const dst_y = src_y +% @as(usize, @bitCast(y_inc));
        if (dst_x > 35) {
            return;
        }
        if (dst_y > 35) {
            return;
        }
        const dst = dst_y * 36 + dst_x;
        const dst_piece = self.squares[dst];
        if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
            return;
        } else {
            buffer.*[buffer.len] = (dst << 12) | src;
            buffer.len += 1;
        }
    }

    fn isSquarePlayable(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize) bool {
        const src_x = src % 36;
        const src_y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        const dst_x = src_x +% @as(usize, @bitCast(x_inc));
        const dst_y = src_y +% @as(usize, @bitCast(y_inc));
        if (dst_x > 35) {
            return false;
        }
        if (dst_y > 35) {
            return false;
        }
        const dst = dst_y * 36 + dst_x;
        const dst_piece = self.squares[dst];
        if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
            return false;
        } else {
            return true;
        }
    }

    fn genLine(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, count: usize, buffer: *[]Move) void {
        var x = src % 36;
        var y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        for (0..count) |_| {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
                return;
            } else {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
                if (dst_piece != not_a_piece) {
                    return;
                }
            }
        }
    }

    fn genHookLine(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, buffer: *[]Move) void {
        var x = src % 36;
        var y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        var count: isize = 1;
        while (true) : (count += 1) {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
                return;
            } else {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
                if (dst_piece != not_a_piece) {
                    return;
                } else {
                    genLineFrom(self, src, x_inc_in * count, y_inc_in * count, y_inc_in, -x_inc_in, 255, buffer);
                    genLineFrom(self, src, x_inc_in * count, y_inc_in * count, -y_inc_in, x_inc_in, 255, buffer);
                }
            }
        }
    }

    fn genJumpAndContinue(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, jump_count: usize, buffer: *[]Move) void {
        var x = src % 36;
        var y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        for (0..jump_count) |_| {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if ((dst_piece & piece_mask) == not_a_piece) {
                break;
            }
        }
        while (true) {
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
                return;
            } else {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
                if (dst_piece != not_a_piece) {
                    return;
                }
            }
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
        }
    }

    fn genOnlyCaptureLine(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, count: usize, buffer: *[]Move) void {
        var x = src % 36;
        var y = src / 36;
        const piece = self.squares[src];
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        for (0..count) |_| {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece != not_a_piece and (piece & color_mask) != (dst_piece & color_mask)) {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
            }
        }
    }

    fn genFlyAndCaptureLine(self: *Self, src: usize, x_inc_in: isize, y_inc_in: isize, count: usize, buffer: *[]Move) void {
        var piece = self.squares[src];
        var priority_list = [_]u32{ // Piece: priority
            0, 0, // king
            1, 0, // prince
            2, 1, // great general
            84, 2, // vice general
            51, 3, // rook general
            50, 3, // bishop general
            82, 3, // violent dragon
            229, 3, // flying crocodile
        };
        var priority: usize = 1000;
        for (0..(priority_list.len / 2)) |i| {
            const curr_piece = priority_list[i << 1];
            const curr_priority = priority_list[(i << 1) | 1];
            if (piece & piece_mask == curr_piece) {
                priority = curr_priority;
            }
        }
        if (priority == 1000) {
            // TODO: confirm that this cant happen!
            log("something horrible happened!", .{});
            // unreachable
            return;
        }
        var x = src % 36;
        var y = src / 36;
        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        for (0..count) |_| {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece == not_a_piece) {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
            } else {
                var dst_priority: usize = 1000;
                for (0..(priority_list.len / 2)) |i| {
                    const curr_piece = priority_list[i << 1];
                    const curr_priority = priority_list[(i << 1) | 1];
                    if (dst_piece & piece_mask == curr_piece) {
                        dst_priority = curr_priority;
                    }
                }
                if (dst_priority <= priority) {
                    return;
                }
            }
        }
    }

    fn genLineFrom(self: *Self, src: usize, from_x_inc_in: isize, from_y_inc_in: isize, x_inc_in: isize, y_inc_in: isize, count: usize, buffer: *[]Move) void {
        const piece = self.squares[src];

        var from_x_inc = from_x_inc_in;
        var from_y_inc = from_y_inc_in;
        if ((piece & color_mask) > 0) {
            from_x_inc = -%from_x_inc;
            from_y_inc = -%from_y_inc;
        }

        var x = src % 36;
        var y = src / 36;
        x +%= @as(usize, @bitCast(from_x_inc));
        y +%= @as(usize, @bitCast(from_y_inc));
        if (x > 35) {
            return;
        }
        if (y > 35) {
            return;
        }

        var x_inc = x_inc_in;
        var y_inc = y_inc_in;
        if ((piece & color_mask) > 0) {
            x_inc = -%x_inc;
            y_inc = -%y_inc;
        }
        for (0..count) |_| {
            x +%= @as(usize, @bitCast(x_inc));
            y +%= @as(usize, @bitCast(y_inc));
            if (x > 35) {
                return;
            }
            if (y > 35) {
                return;
            }
            const dst = y * 36 + x;
            const dst_piece = self.squares[dst];
            if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
                return;
            } else {
                buffer.*[buffer.len] = (dst << 12) | src;
                buffer.len += 1;
                if (dst_piece != not_a_piece) {
                    return;
                }
            }
        }
    }

    fn checkIgui(self: *Self, src: usize, inc1_x_in: isize, inc1_y_in: isize, inc2_x_in: isize, inc2_y_in: isize, buffer: *[]Move) void {
        const piece = self.squares[src];

        var inc1_x = inc1_x_in;
        var inc1_y = inc1_y_in;
        var inc2_x = inc2_x_in;
        var inc2_y = inc2_y_in;
        if ((piece & color_mask) > 0) {
            inc1_x = -%inc1_x;
            inc1_y = -%inc1_y;
            inc2_x = -%inc2_x;
            inc2_y = -%inc2_y;
        }

        var midx = (src % 36) +% @as(usize, @bitCast(inc1_x));
        var midy = (src / 36) +% @as(usize, @bitCast(inc1_y));
        if (midx > 35 or midy > 35) {
            return;
        }
        const mid_piece = self.squares[midy * 36 + midx];
        if (mid_piece != not_a_piece and (piece & color_mask) == (mid_piece & color_mask)) {
            return;
        }

        var dstx = midx +% @as(usize, @bitCast(inc2_x));
        var dsty = midy +% @as(usize, @bitCast(inc2_y));
        if (dstx > 35 or dsty > 35) {
            return;
        }
        const dst = dsty * 36 + dstx;
        const dst_piece = self.squares[dst];
        if (dst == src) {
            // real igui 💯💯💯💯💯💯💯💯💯
        } else if (dst_piece != not_a_piece and (piece & color_mask) == (dst_piece & color_mask)) {
            return;
        }

        // opposite inc + 1
        var igui_x = @as(usize, @bitCast(1 - inc2_x));
        var igui_y = @as(usize, @bitCast(1 - inc2_y));
        buffer.*[buffer.len] = (dst << 12) | src | is_igui_mask | (igui_x << 25) | (igui_y << 27);
        buffer.len += 1;
    }

    fn genKnight(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, -1, -2, buffer);
        checkSquare(self, square, 0, -2, buffer);
    }

    fn genFlyingDragon(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, 2, -2, buffer);
        checkSquare(self, square, 2, 2, buffer);
        checkSquare(self, square, -2, -2, buffer);
        checkSquare(self, square, -2, 2, buffer);
    }

    fn genKirin(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, 1, -1, buffer);
        checkSquare(self, square, 1, 1, buffer);
        checkSquare(self, square, -1, -1, buffer);
        checkSquare(self, square, -1, 1, buffer);

        checkSquare(self, square, -2, 0, buffer);
        checkSquare(self, square, 2, 0, buffer);
    }

    fn genPhoenix(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, 1, 0, buffer);
        checkSquare(self, square, -1, 0, buffer);
        checkSquare(self, square, 0, -1, buffer);
        checkSquare(self, square, 0, 1, buffer);

        genFlyingDragon(self, square, buffer);
    }

    fn genFlyingCat(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, 3, -3, buffer);
        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, -3, -3, buffer);
        checkSquare(self, square, 3, 0, buffer);
        checkSquare(self, square, -3, 0, buffer);

        checkSquare(self, square, -1, 1, buffer);
        checkSquare(self, square, 0, 1, buffer);
        checkSquare(self, square, 1, 1, buffer);
    }

    fn genRunningHorse(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, 1, buffer);

        checkSquare(self, square, 2, 2, buffer);
        checkSquare(self, square, -2, 2, buffer);
    }

    fn genMountainFalcon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        checkSquare(self, square, 0, -2, buffer);

        genLine(self, square, -1, 1, 2, buffer);
        genLine(self, square, 1, 1, 2, buffer);
    }
    fn genLittleTurtle(self: *Self, square: usize, buffer: *[]Move) void {
        checkSquare(self, square, 0, -2, buffer);
        checkSquare(self, square, 0, 2, buffer);
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, 0, 2, buffer);
        genLine(self, square, 1, 0, 2, buffer);
    }

    fn genGreatStag(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);

        checkSquare(self, square, 2, -2, buffer);
        checkSquare(self, square, -2, -2, buffer);
    }

    fn genLeftMountainEagle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genLine(self, square, 1, 1, 2, buffer);

        checkSquare(self, square, -2, -2, buffer);
        checkSquare(self, square, -2, 2, buffer);
    }

    fn genRightMountainEagle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genLine(self, square, -1, 1, 2, buffer);

        checkSquare(self, square, 2, -2, buffer);
        checkSquare(self, square, 2, 2, buffer);
    }

    fn genKirinMasterOrGreatTurtle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);

        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);

        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, 0, 3, buffer);
    }

    fn genPhoenixMaster(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);

        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);

        checkSquare(self, square, 3, -3, buffer);
        checkSquare(self, square, -3, -3, buffer);
    }

    fn genGreatMaster(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);

        genLine(self, square, 1, 1, 5, buffer);
        genLine(self, square, -1, 1, 5, buffer);

        genLine(self, square, 1, 0, 5, buffer);
        genLine(self, square, -1, 0, 5, buffer);

        checkSquare(self, square, 3, -3, buffer);
        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, -3, -3, buffer);
    }

    fn genHornedFalcon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -2, buffer);
    }

    fn genSoaringEagle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 2, -2, buffer);
        checkSquare(self, square, -2, -2, buffer);
    }

    fn genRoaringDog(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 3, buffer);
        genLine(self, square, 1, 1, 3, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, 0, 3, buffer);
        checkSquare(self, square, 3, 0, buffer);
        checkSquare(self, square, -3, 0, buffer);
    }

    fn genLionDog(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, 0, 3, buffer);
        checkSquare(self, square, 3, 0, buffer);
        checkSquare(self, square, -3, 0, buffer);

        checkSquare(self, square, -3, -3, buffer);
        checkSquare(self, square, 3, -3, buffer);
        checkSquare(self, square, -3, 3, buffer);
        checkSquare(self, square, 3, 3, buffer);
    }

    fn genGreatDreamEater(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 3, 0, buffer);
        checkSquare(self, square, -3, 0, buffer);
    }

    fn genHeavenlyHorse(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);

        checkSquare(self, square, 1, -2, buffer);
        checkSquare(self, square, -1, -2, buffer);
        checkSquare(self, square, 1, 2, buffer);
        checkSquare(self, square, -1, 2, buffer);
    }

    fn genSpiritTurtle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -3, buffer);
        checkSquare(self, square, 0, 3, buffer);
        checkSquare(self, square, 3, 0, buffer);
        checkSquare(self, square, -3, 0, buffer);
    }

    fn genTreasureTurtle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -2, buffer);
        checkSquare(self, square, 0, 2, buffer);
        checkSquare(self, square, 2, 0, buffer);
        checkSquare(self, square, -2, 0, buffer);
    }

    fn genWoodenDove(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 2, buffer);
        genLine(self, square, 0, 1, 2, buffer);
        genLine(self, square, 1, 0, 2, buffer);
        genLine(self, square, -1, 0, 2, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        // might repeat moves
        genLineFrom(self, square, 2, -2, 1, -1, 3, buffer);
        genLineFrom(self, square, -2, -2, -1, -1, 3, buffer);
        genLineFrom(self, square, 2, 2, 1, 1, 3, buffer);
        genLineFrom(self, square, -2, 2, -1, 1, 3, buffer);
    }

    fn genCenterMaster(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);

        genLine(self, square, -1, 1, 3, buffer);
        genLine(self, square, 1, 1, 3, buffer);

        genLineFrom(self, square, 0, -1, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 1, 0, 1, 255, buffer);
        genLineFrom(self, square, 1, -1, 1, -1, 255, buffer);
        genLineFrom(self, square, -1, -1, -1, -1, 255, buffer);

        genLineFrom(self, square, 0, -3, 0, 1, 255, buffer);
        genLineFrom(self, square, 3, -3, -1, 1, 255, buffer);
        genLineFrom(self, square, -3, -3, 1, 1, 255, buffer);
    }

    fn genRocMaster(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 5, buffer);
        genLine(self, square, -1, 0, 5, buffer);

        genLine(self, square, -1, 1, 5, buffer);
        genLine(self, square, 1, 1, 5, buffer);

        genLineFrom(self, square, 2, -2, 1, -1, 255, buffer);
        genLineFrom(self, square, -2, -2, -1, -1, 255, buffer);
    }

    // bitfields!!!! PLS WHERE ARE YOU???
    fn genFreeEagle(self: *Self, square: usize, buffer: *[]Move) void {
        // forwards
        genLineFrom(self, square, 1, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, 2, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, -1, 0, -1, 0, 255, buffer);
        genLineFrom(self, square, -2, 0, -1, 0, 255, buffer);
        genLineFrom(self, square, 0, -1, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, -2, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 1, 0, 1, 255, buffer);
        genLineFrom(self, square, 0, 2, 0, 1, 255, buffer);
        // backwards
        genLineFrom(self, square, 4, 0, -1, 0, 255, buffer);
        genLineFrom(self, square, 3, 0, -1, 0, 255, buffer);
        genLineFrom(self, square, -4, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, -3, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, 0, -4, 0, 1, 255, buffer);
        genLineFrom(self, square, 0, -3, 0, 1, 255, buffer);
        genLineFrom(self, square, 0, 4, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 3, 0, -1, 255, buffer);
        // forwards
        genLineFrom(self, square, 3, -3, 1, -1, 255, buffer);
        genLineFrom(self, square, 2, -2, 1, -1, 255, buffer);
        genLineFrom(self, square, 1, -1, 1, -1, 255, buffer);
        genLineFrom(self, square, -3, -3, -1, -1, 255, buffer);
        genLineFrom(self, square, -2, -2, -1, -1, 255, buffer);
        genLineFrom(self, square, -1, -1, -1, -1, 255, buffer);
        genLineFrom(self, square, 3, 3, 1, 1, 255, buffer);
        genLineFrom(self, square, 2, 2, 1, 1, 255, buffer);
        genLineFrom(self, square, 1, 1, 1, 1, 255, buffer);
        genLineFrom(self, square, -3, 3, -1, 1, 255, buffer);
        genLineFrom(self, square, -2, 2, -1, 1, 255, buffer);
        genLineFrom(self, square, -1, 1, -1, 1, 255, buffer);
        // backwards
        genLineFrom(self, square, 4, -4, -1, 1, 255, buffer);
        genLineFrom(self, square, 3, -3, -1, 1, 255, buffer);
        genLineFrom(self, square, -4, -4, 1, 1, 255, buffer);
        genLineFrom(self, square, -3, -3, 1, 1, 255, buffer);
        genLineFrom(self, square, -4, 4, 1, -1, 255, buffer);
        genLineFrom(self, square, -3, 3, 1, -1, 255, buffer);
        genLineFrom(self, square, 4, 4, -1, -1, 255, buffer);
        genLineFrom(self, square, 3, 3, -1, -1, 255, buffer);
    }

    fn genFreeBird(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);
        genLine(self, square, -1, 1, 3, buffer);
        genLine(self, square, 1, 1, 3, buffer);

        genJumpAndContinue(self, square, 1, -1, 3, buffer);
        genJumpAndContinue(self, square, -1, -1, 3, buffer);
    }

    fn genGreatFalcon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);

        genLineFrom(self, square, 0, -1, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 1, 0, 1, 255, buffer);
    }

    fn genAncientDragon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genOnlyCaptureLine(self, square, 0, -1, 255, buffer);
        genOnlyCaptureLine(self, square, 0, 1, 255, buffer);
    }

    fn genTeachingKing(self: *Self, square: usize, buffer: *[]Move) void {
        genLineFrom(self, square, 0, -2, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 2, 0, 1, 255, buffer);
        genLineFrom(self, square, 2, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, -2, 0, -1, 0, 255, buffer);

        genLineFrom(self, square, 2, -2, 1, -1, 255, buffer);
        genLineFrom(self, square, 2, 2, 1, 1, 255, buffer);
        genLineFrom(self, square, -2, -2, -1, -1, 255, buffer);
        genLineFrom(self, square, -2, 2, -1, 1, 255, buffer);
    }

    fn genMountainCrane(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genTeachingKing(self, square, buffer);
    }

    fn genGreatEagle(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genLineFrom(self, square, 0, -1, 0, -1, 255, buffer);
        genLineFrom(self, square, 0, 1, 0, 1, 255, buffer);
        genLineFrom(self, square, 1, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, -1, 0, -1, 0, 255, buffer);

        genLineFrom(self, square, 1, -1, 1, -1, 255, buffer);
        genLineFrom(self, square, 1, 1, 1, 1, 255, buffer);
        genLineFrom(self, square, -1, -1, -1, -1, 255, buffer);
        genLineFrom(self, square, -1, 1, -1, 1, 255, buffer);
    }

    fn genGreatElephant(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, -1, 3, buffer);
        genLine(self, square, -1, -1, 3, buffer);

        genJumpAndContinue(self, square, 0, -1, 1, buffer);
        genJumpAndContinue(self, square, 0, 1, 1, buffer);
        genJumpAndContinue(self, square, 1, 0, 1, buffer);
        genJumpAndContinue(self, square, -1, 0, 1, buffer);

        genJumpAndContinue(self, square, -1, 1, 3, buffer);
        genJumpAndContinue(self, square, 1, 1, 3, buffer);
    }

    fn genGoldenBird(self: *Self, square: usize, buffer: *[]Move) void {
        genJumpAndContinue(self, square, -1, -1, 3, buffer);
        genJumpAndContinue(self, square, 1, -1, 3, buffer);

        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);

        genLine(self, square, 1, 1, 3, buffer);
        genLine(self, square, -1, 1, 3, buffer);
    }

    fn genRainDemon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, 0, 2, buffer);
        genLine(self, square, -1, 0, 2, buffer);
        genLine(self, square, 0, -1, 3, buffer);
        genLine(self, square, 0, 1, 255, buffer);

        genOnlyCaptureLine(self, square, 1, -1, 255, buffer);
        genOnlyCaptureLine(self, square, -1, -1, 255, buffer);
    }

    fn genRookGeneral(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, 0, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 0, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 0, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, 0, 255, buffer);
    }

    fn genBishopGeneral(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, -1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, -1, 255, buffer);
    }

    fn genGreatGeneral(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, -1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, -1, 255, buffer);

        genFlyAndCaptureLine(self, square, 0, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 0, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 0, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, 0, 255, buffer);
    }

    fn genViolentDragon(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, -1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, -1, 255, buffer);

        genLine(self, square, 0, -1, 2, buffer);
        genLine(self, square, 0, 1, 2, buffer);
        genLine(self, square, 1, 0, 2, buffer);
        genLine(self, square, -1, 0, 2, buffer);
    }

    fn genFlyingCrocodile(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, 0, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 0, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 0, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, 1, -1, 3, buffer);
        genLine(self, square, -1, -1, 3, buffer);
        genLine(self, square, 1, 1, 2, buffer);
        genLine(self, square, -1, 1, 2, buffer);
    }

    fn genViceGeneral(self: *Self, square: usize, buffer: *[]Move) void {
        genFlyAndCaptureLine(self, square, -1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, 1, 255, buffer);
        genFlyAndCaptureLine(self, square, -1, -1, 255, buffer);
        genFlyAndCaptureLine(self, square, 1, -1, 255, buffer);

        checkSquare(self, square, 0, -2, buffer);
        checkSquare(self, square, 0, 2, buffer);
        checkSquare(self, square, 2, 0, buffer);
        checkSquare(self, square, -2, 0, buffer);
    }

    fn genHookMover(self: *Self, square: usize, buffer: *[]Move) void {
        genHookLine(self, square, 0, -1, buffer);
        genHookLine(self, square, 0, 1, buffer);
        genHookLine(self, square, 1, 0, buffer);
        genHookLine(self, square, -1, 0, buffer);
    }

    fn genLongNosedGoblin(self: *Self, square: usize, buffer: *[]Move) void {
        genHookLine(self, square, -1, 1, buffer);
        genHookLine(self, square, 1, 1, buffer);
        genHookLine(self, square, -1, -1, buffer);
        genHookLine(self, square, 1, -1, buffer);
    }

    fn genCapricorn(self: *Self, square: usize, buffer: *[]Move) void {
        genHookLine(self, square, -1, 1, buffer);
        genHookLine(self, square, 1, 1, buffer);
        genHookLine(self, square, -1, -1, buffer);
        genHookLine(self, square, 1, -1, buffer);
    }

    fn genPeacock(self: *Self, square: usize, buffer: *[]Move) void {
        genHookLine(self, square, -1, 1, buffer);
        genHookLine(self, square, 1, 1, buffer);
        genHookLine(self, square, -1, -1, buffer);
        genHookLine(self, square, 1, -1, buffer);

        genLine(self, square, -1, 1, 2, buffer);
        genLine(self, square, 1, 1, 2, buffer);
    }

    fn genFreeDemon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, 1, 5, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genLine(self, square, 1, 0, 255, buffer);
    }

    fn genViolentBear(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, 0, 1, buffer);
        genLine(self, square, -1, 0, 1, buffer);
        genLine(self, square, 0, -1, 1, buffer);
        genLine(self, square, 1, -1, 2, buffer);
        genLine(self, square, -1, -1, 2, buffer);
    }

    fn genEarthDragon(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, -1, 2, buffer);
        genLine(self, square, -1, -1, 2, buffer);
        genLine(self, square, 0, -1, 1, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
    }

    fn genTreacherousFox(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);

        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
    }

    fn genDivineSparrow(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, 1, 1, buffer);
        genLine(self, square, 0, -1, 1, buffer);
        genLine(self, square, 1, 0, 1, buffer);
        genLine(self, square, -1, 0, 1, buffer);

        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
    }

    fn genHeavenlyTetrachKing(self: *Self, square: usize, buffer: *[]Move) void {
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);

        genLineFrom(self, square, 1, 0, 1, 0, 255, buffer);
        genLineFrom(self, square, -1, 0, -1, 0, 255, buffer);
        genLineFrom(self, square, 0, 1, 0, 1, 255, buffer);
        genLineFrom(self, square, 0, -1, 0, -1, 255, buffer);

        genLineFrom(self, square, -1, -1, -1, -1, 255, buffer);
        genLineFrom(self, square, 1, -1, 1, -1, 255, buffer);
        genLineFrom(self, square, -1, 1, -1, 1, 255, buffer);
        genLineFrom(self, square, 1, 1, 1, 1, 255, buffer);

        if (isSquarePlayable(self, square, 1, 0)) {
            var x: Move = 1 + 1;
            var y: Move = 0 + 1;
            buffer.*[buffer.len] = (square << 12) | square | is_igui_mask | (x << 25) | (y << 27);
            buffer.len += 1;
        }
        if (isSquarePlayable(self, square, -1, 0)) {
            var x: Move = -1 +% 1;
            var y: Move = 0 + 1;
            buffer.*[buffer.len] = (square << 12) | square | is_igui_mask | (x << 25) | (y << 27);
            buffer.len += 1;
        }
        if (isSquarePlayable(self, square, 0, 1)) {
            var x: Move = 0 + 1;
            var y: Move = 1 + 1;
            buffer.*[buffer.len] = (square << 12) | square | is_igui_mask | (x << 25) | (y << 27);
            buffer.len += 1;
        }
        if (isSquarePlayable(self, square, 0, -1)) {
            var x: Move = 0 + 1;
            var y: Move = -1 +% 1;
            buffer.*[buffer.len] = (square << 12) | square | is_igui_mask | (x << 25) | (y << 27);
            buffer.len += 1;
        }
    }

    fn genLion(self: *Self, square: usize, buffer: *[]Move) void {
        for (0..5) |xi| {
            for (0..5) |yi| {
                if (xi == 2 and yi == 2) {
                    continue;
                }
                var x_diff: isize = @as(isize, @intCast(xi)) - 2;
                var y_diff: isize = @as(isize, @intCast(yi)) - 2;
                checkSquare(self, square, x_diff, y_diff, buffer);
            }
        }
        for (0..3) |xi| {
            for (0..3) |yi| {
                if (xi == 1 and yi == 1) {
                    continue;
                }
                var xdiff: isize = @as(isize, @intCast(xi)) - 1;
                var ydiff: isize = @as(isize, @intCast(yi)) - 1;
                for (0..3) |x2i| {
                    for (0..3) |y2i| {
                        if (x2i == 1 and y2i == 1) {
                            continue;
                        }
                        var x2diff: isize = @as(isize, @intCast(x2i)) - 1;
                        var y2diff: isize = @as(isize, @intCast(y2i)) - 1;
                        checkIgui(self, square, xdiff, ydiff, x2diff, y2diff, buffer);
                    }
                }
            }
        }
    }

    fn genFuriousFiend(self: *Self, square: usize, buffer: *[]Move) void {
        genLion(self, square, buffer);

        genLine(self, square, 0, 1, 3, buffer);
        genLine(self, square, 0, -1, 3, buffer);
        genLine(self, square, 1, 0, 3, buffer);
        genLine(self, square, -1, 0, 3, buffer);

        genLine(self, square, -1, -1, 3, buffer);
        genLine(self, square, 1, -1, 3, buffer);
        genLine(self, square, -1, 1, 3, buffer);
        genLine(self, square, 1, 1, 3, buffer);
    }

    fn genBuddhistSpirit(self: *Self, square: usize, buffer: *[]Move) void {
        genLion(self, square, buffer);

        genLine(self, square, 0, 1, 255, buffer);
        genLine(self, square, 0, -1, 255, buffer);
        genLine(self, square, 1, 0, 255, buffer);
        genLine(self, square, -1, 0, 255, buffer);

        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);
    }

    fn genLionHawk(self: *Self, square: usize, buffer: *[]Move) void {
        genLion(self, square, buffer);

        genLine(self, square, -1, -1, 255, buffer);
        genLine(self, square, 1, -1, 255, buffer);
        genLine(self, square, -1, 1, 255, buffer);
        genLine(self, square, 1, 1, 255, buffer);

        genLineFrom(self, square, -1, -1, -1, -1, 255, buffer);
        genLineFrom(self, square, 1, -1, 1, -1, 255, buffer);
        genLineFrom(self, square, -1, 1, -1, 1, 255, buffer);
        genLineFrom(self, square, 1, 1, 1, 1, 255, buffer);
    }

    fn internalCalculatePieceLegalMoves(self: *Self, square: usize, buffer: *[]Move) void {
        var x = square % 36;
        var y = square / 36;
        const piece = self.squares[square];
        const piece_color_mask = piece & color_mask;

        // does NOT care about promotions since they are done
        // automatically in makeMove()
        switch (piece & piece_mask) {
            69 => self.genKnight(square, buffer),
            113 => self.genFlyingDragon(square, buffer),
            121 => self.genKirin(square, buffer),
            120 => self.genPhoenix(square, buffer),
            90 => self.genFlyingCat(square, buffer),
            13 => self.genRunningHorse(square, buffer),
            91 => self.genMountainFalcon(square, buffer),
            123 => self.genLittleTurtle(square, buffer),
            81 => self.genGreatStag(square, buffer),
            17 => self.genLeftMountainEagle(square, buffer),
            16 => self.genRightMountainEagle(square, buffer),
            66, 124 => self.genKirinMasterOrGreatTurtle(square, buffer),
            65 => self.genPhoenixMaster(square, buffer),
            185 => self.genGreatMaster(square, buffer),
            198 => self.genHornedFalcon(square, buffer),
            199 => self.genSoaringEagle(square, buffer),
            204 => self.genRoaringDog(square, buffer),
            205 => self.genLionDog(square, buffer),
            255 => self.genGreatDreamEater(square, buffer),
            262 => self.genHeavenlyHorse(square, buffer),
            258 => self.genSpiritTurtle(square, buffer),
            257 => self.genTreasureTurtle(square, buffer),
            9 => self.genWoodenDove(square, buffer),
            145 => self.genCenterMaster(square, buffer),
            146 => self.genRocMaster(square, buffer),
            165 => self.genFreeEagle(square, buffer),
            248 => self.genFreeBird(square, buffer),
            293 => self.genGreatFalcon(square, buffer),
            250 => self.genTeachingKing(square, buffer),
            268 => self.genMountainCrane(square, buffer),
            294 => self.genGreatEagle(square, buffer),
            299 => self.genGreatElephant(square, buffer),
            101 => self.genGoldenBird(square, buffer),
            247 => self.genAncientDragon(square, buffer),
            228 => self.genRainDemon(square, buffer),
            51 => self.genRookGeneral(square, buffer),
            50 => self.genBishopGeneral(square, buffer),
            2 => self.genGreatGeneral(square, buffer),
            82 => self.genViolentDragon(square, buffer),
            229 => self.genFlyingCrocodile(square, buffer),
            84 => self.genViceGeneral(square, buffer),
            122 => self.genHookMover(square, buffer),
            15 => self.genLongNosedGoblin(square, buffer),
            125 => self.genCapricorn(square, buffer),
            61 => self.genPeacock(square, buffer),
            278 => self.genHeavenlyTetrachKing(square, buffer),
            79 => self.genLion(square, buffer),
            239 => self.genFuriousFiend(square, buffer),
            249 => self.genBuddhistSpirit(square, buffer),
            166 => self.genLionHawk(square, buffer),
            12 => self.genFreeDemon(square, buffer),
            131 => self.genViolentBear(square, buffer),
            144 => self.genTreacherousFox(square, buffer),
            270 => self.genDivineSparrow(square, buffer),
            11 => self.genEarthDragon(square, buffer),

            else => {
                const description = piece_descriptions[piece & piece_mask];
                if (std.mem.eql(u8, description.directions[0..4], "null")) {
                    // TODO: test that this never happens
                    log("OH GOD NO: {}", .{piece & piece_mask});
                    //unreachable;
                    return;
                }
                for (0..3) |y_2| {
                    for (0..3) |x_2| {
                        if (x_2 == 1 and y_2 == 1) {
                            continue;
                        }
                        var inc_x = x_2 -% 1;
                        var inc_y = y_2 -% 1;
                        if (piece_color_mask > 0) {
                            inc_x = -%inc_x;
                            inc_y = -%inc_y;
                        }
                        var curr_x: usize = x;
                        var curr_y: usize = y;
                        const di = y_2 * 3 + x_2;
                        const direction = description.directions[di];
                        for (0..direction) |_| {
                            curr_x +%= inc_x;
                            curr_y +%= inc_y;
                            // also takes care of (int)val < 0
                            const bounds_x = curr_x > 35;
                            const bounds_y = curr_y > 35;

                            if (bounds_x or bounds_y) {
                                break;
                            }
                            var curr_square = curr_y * 36 + curr_x;
                            var move = (curr_square << 12) | square;
                            if (self.squares[curr_square] == not_a_piece) {
                                buffer.*[buffer.len] = move;
                                buffer.len += 1;
                                continue;
                            }
                            if (self.squares[curr_square] & color_mask == piece_color_mask) {
                                break;
                            } else {
                                buffer.*[buffer.len] = move;
                                buffer.len += 1;
                                break;
                            }
                        }
                    }
                }
            },
        }
    }

    pub fn calculatePieceLegalMoves(self: *Self, square: usize, buffer: *[]Move) usize {
        buffer.len = 0;
        internalCalculatePieceLegalMoves(self, square, buffer);
        return buffer.len;
    }

    // works on a move buffer
    pub fn calculateAllLegalMoves(self: *Self, buffer: *[]Move, gote: bool) usize {
        _ = gote;
        buffer.len = 0;
        for (self.squares, 0..) |_, index| {
            self.internalCalculatePieceLegalMoves(index, buffer);
        }
        return buffer.len;
    }
};
